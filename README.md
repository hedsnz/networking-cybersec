# Networking and cybersecurity

These are some collected notes about the basics of networking and cybersecurity.

The current primary source is Professor Messer's [CompTIA Network+ training series](https://www.professormesser.com/).