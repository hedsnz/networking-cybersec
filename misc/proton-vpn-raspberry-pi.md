## Install ProtonVPN with OpenVPN on Raspberry Pi OS (Raspberry Pi 4B)

https://protonvpn.com/support/linux-vpn-setup/

Install openvpn

```
sudo apt install openvpn
```

```
sudo apt install network-manager-openvpn-gnome resolvconf
```

Download server config files as per instructions

In the dir of your downloaded config file (`~/Downloads` probably), run

```
sudo openvpn <name.of.config.ovpn>
```

Enter your auth username and password. These are your OpenVPN
credentials from the ProtonVPN dashboard.