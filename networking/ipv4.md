- 5 classes (A, B, C, D and E)

- classes A, B and C are reserved for public and private use.

- public IP address: found on the internet
- private IP address: internal network, e.g., home
network, corporate network

- class D: used for multicasting. this is not a
very common thing. e.g. used for conference calling,
brings lots of people together at once.

- class E: used for government or research.

- 32 bits in length (i.e. 32 1s and 0s)
broken down into four 8-bit sections (octets)
four 8-bit octets


part of the IP address is the Network ID,
which says where you're at (i.e. the network that
you're on, like a phone's area code). everyone on
the same network will have the same network ID,
but the rest of it will be unique.

classes

"node" and "host" IDs are the same thing.

History:
- Class A were for the largest organisations e.g.,
governments etc.
- Class B were medium-sized entities/networks
- Class C were for small networks, e.g. small
schools etc.

There are very few Class A addresses, but each
network has a bunch of available IPs.
There are a medium number of Class B addresses, each 
with a medium number of availbale IPs.
And there are heaps of Class C networks, but 
each has not many available IPs.

To tell the difference between Class A, B and C,
you have to look at the first octet (i.e., first
three digits before the period).

Class A first octet: 1--126
Class B first octet: 128--191
Class C first octet: 192--223

of course, there is 127.0.0.1 which
is the local loopback or loopback address.
when you use it, it means you're talking to yourself.
e.g. if you can ping yourself, you can be sure
that your network card is working.

Class A: first octet is the Network ID,
while the last 3 octets are the node/host ID

Class B: first two octets are the Network ID,
while the last 2 octets are the nost/host ID.

Class C: the first three octets are the Network
ID, while the last octet is the node/host ID.

So only the first octet is limited with respect
to the numbers they can take on. The last three
octets (of all classes A B and C) can be anywhere
from 0-255.
