# hostname
show the system's hostname

## examples

return the actual PC name
`hostname`

return your IP address
`hostname -I`

# nmap
scan for open ports. pass target in as IP address or host name (if locally, e.g., output of `hostname`)

## examples
find local open ports
`nmap 192.168.0.0`

add the `-v` flag for more verbose output
`nmap -v 192.168.0.0`

add the `-sV` flag to probe open ports to determine service/version into