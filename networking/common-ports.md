# Common ports

## Telnet

- Telecommunication Network
- tcp/23
- log in to devices remotely
- console access
- in-the-clear communication (NOT ENCRYPTED)
- not a good choice for production systems

## SSH - secure shell

- encrypted communication link
- tcp/22
- looks and acts the same as Telnet

## DNS - Domain Name System

- converts names to IP addresses
- e.g., www.professormesser.com = 162.159.246.164
... when you navigate to that site, your machine reaches out to
the DNS server to request the IP address
- udp/53

## SMTP - Simple Mail Transfer Protocol

- server-to-server email transfer
- tcp/25
- also user to send mail from a device to a mail server (commonly
configured on mobile devices and email clients)
- other protocols are used for clients to receive email (IMAP, POP3)

## SFTP - Secure FTP

- uses the SSH File Transfer Protocol
- tcp/22 (i.e., same as SSH)
- provides file system functionality (resuming interrupted transfers,
directory listings, remote file removal)
- encrypted communication using SSH

## File transfer application protocols

### FTP - File Transfer Protocol

- tcp/20 (active mode data), tcp/21 (control)
- transfers files between systems
- authenticates with a username and password
- full-featured functionality (list, add, delete, etc.)

### TFTP - Trivial File Transfer Protocol

- udp/69
- very simple file transfer application (read files and write files)
- no authentication (not used on production systems)

## DHCP - Dynamic Host Configuration Protocol

- when you start your computer, it's able to get an IP address automatically
- automated configuration of IP address, subnet mask and other options
- udp/67, udp/68
- requires a DHCP server (server, appliance, integrated into a SOHO router, etc.)
- dynamic/pooled: IP address are assigned in real time from a pool.
Each system is given a lease and must renew at set intervals
- DHCP reservation: addresses are assigned by MAC address in the DHCP server. Common to do this with servers. Quickly manage addresses from one location.
