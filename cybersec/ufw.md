# Configure a UFW firewall for home desktop

## Install

```
sudo apt install ufw
```

## Set defaults

```
sudo ufw default deny incoming
sudo ufw default allow outgoing
```

## References

- https://linuxconfig.org/how-to-install-ufw-and-use-it-to-set-up-a-basic-firewall